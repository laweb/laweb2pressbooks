from setuptools import find_packages, setup
from laweb2pressbooks import __version__

setup(
    name="laweb2pressbooks",
    version=__version__,
    author="Bengt Giger",
    author_email="bgiger@ethz.ch",
    description="Converter from LaWeB HTML to Pressbooks XML",
    packages=find_packages(),
    py_modules=["laweb2pressbooks"],
    package_data={"": ["laweb2pressbooks.css", "templates/*.j2"]},
    entry_points={
        "console_scripts": [
            "laweb2pressbooks = laweb2pressbooks.__main__:parse",
        ]
    },
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Topic:: Scientific / Engineering:: Mathematics",
        "Topic :: Text Processing :: Markup :: HTML",
        "Topic :: Text Processing :: Markup :: XML",
        "Topic :: Text Processing :: Markup :: LaTeX",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    url="https://gitlab.ethz.ch/laweb/laweb2pressbooks",
    install_requires=["click", "beautifulsoup4", "jinja2", "Pillow", "requests"],
)
