"""
This file is part of laweb2pressbooks.

laweb2pressbooks is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

laweb2pressbooks is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with laweb2pressbooks.  If not, see <https://www.gnu.org/licenses/>.
"""

import click
import logging
import laweb2pressbooks.laweb2pressbooks
from laweb2pressbooks import __version__


@click.command()
@click.argument("input")
@click.option(
    "--output", default="export.xml", help="XML output file for Pressbooks import"
)
@click.option(
    "--debug", default=False, type=bool, is_flag=True, help="Enable debugging output"
)
@click.option("--quiet", default=False, type=bool, is_flag=True, help="Suppress output")
@click.option("--cssfile", help="Additional CSS formats")
@click.option(
    "--external_images",
    default=False,
    type=bool,
    is_flag=True,
    help="Generated HTML refers to external images",
)
@click.option("--image_width", default=524, type=int, help="Image base width")
@click.option("--url_prefix", help="Prefix prepended to external image URLs")
@click.version_option()
def parse(
    input,
    output,
    debug,
    quiet,
    cssfile="",
    external_images=False,
    image_width=564,
    url_prefix="",
):
    """Convert HTML file INPUT to Wordpress/Pressbooks XML {0}""".format(__version__)

    if debug:
        logging.getLogger().setLevel(logging.DEBUG)
    elif quiet:
        logging.getLogger().setLevel(logging.ERROR)
    else:
        logging.getLogger().setLevel(logging.INFO)

    html_file = open(input)
    lw = laweb2pressbooks.laweb2pressbooks.Laweb(
        html_file, cssfile, external_images, image_width, url_prefix
    )
    lw.parse(external_images)

    logging.info("Writing Wordpress XML")
    with open(output, "w", encoding="utf-8") as f:
        f.write(str(lw))


# Silence noisy PIL logger
pil_logger = logging.getLogger("PIL")
pil_logger.setLevel(logging.INFO)
parse()
