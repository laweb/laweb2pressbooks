"""
This file is part of laweb2pressbooks.

laweb2pressbooks is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

laweb2pressbooks is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with laweb2pressbooks.  If not, see <https://www.gnu.org/licenses/>.
"""

import bs4
from bs4 import BeautifulSoup
from bs4.element import Comment, Tag
from jinja2 import Environment, PackageLoader, select_autoescape
from PIL import Image
from io import BytesIO
from pathlib import Path
import base64
import logging
import re
import os.path
import requests

# Standard width for Pressbooks theme
html_base_width = 564


def clean_raw(text):
    text = text.replace("\n", " ")
    text = text.replace("&ApplyFunction;", " ")
    text = text.replace(": : </span><span", ":</span> <span")
    return text


def tag_content(tag, content=""):
    if isinstance(tag, Tag):

        for item in tag.contents:
            if isinstance(item, Tag) and len(item.contents) > 0:
                content = content + tag_content(item, content)
            else:
                return content + str(tag)
    else:
        content = content + tag

    return content


class Img(object):
    image_padding = (
        12  # size is relative to parent container, padding has to be subtracted
    )
    # image_base_width = 512  # image size (real size: width - padding)

    embedded_max_width = 512
    MAX_ALT_TEXT = 128

    # Size limit for embedded images
    # Sizes enforced by user
    user_height = -1
    user_width = -1
    user_scale = -1
    # Sizes from physical image
    src_width = -1
    src_height = -1

    # final calculated sizes
    width = -1
    height = -1

    def __init__(self, tag, image_base_width):

        self.tag = tag
        # Delete bogus size attributes from tex4ht
        del self.tag["height"]
        del self.tag["width"]

        # Image size calculation for HTML display
        #
        # Either standard (no size attributes), or from values encoded in surrounding tag
        # Check if we are inside of a sizing tag
        if (
            "class" in self.tag.parent.attrs.keys()
            and "mefigcentered" in self.tag.parent["class"]  # noqa: W503
        ):
            # read extra information contained in element id
            if "id" in self.tag.parent.attrs.keys():
                for arg in self.tag.parent["id"].split("&"):
                    # for now, we are only looking for size information
                    size_op, size_arg = arg.split("=")
                    # size given in pixel
                    if size_op == "wpsize":
                        if size_arg.find("x") > 0:
                            self.user_width, self.user_height = size_arg.split("x")
                            self.user_width = int(self.user_width)
                            self.user_height = int(self.user_height)
                        else:
                            self.user_width = int(size_arg)
                    # size given as scaling factor
                    elif size_op == "wpscale":
                        self.user_scale = float(size_arg)

        # Determine size of physical image
        if tag["src"].endswith(".pdf"):
            self.img = Image.open(tag["src"].replace(".pdf", ".png"))
        else:
            self.img = Image.open(tag["src"])
        self.src_width, self.src_height = self.img.size

        if self.user_width > 0:
            self.tag["width"] = int(self.user_width)
            self.width = int(self.user_width - self.image_padding)
            if self.user_height > 0:
                self.tag["height"] = int(self.user_height)
                self.height = self.user_height

        # if size is enforced as ratio
        if self.user_scale > 0:
            self.width = (self.user_scale * image_base_width) - self.image_padding
            self.tag["width"] = "{0}px".format(int(self.width))

        # no size enforced, standard
        if self.user_width < 0 and self.user_scale < 0:
            # self.tag["width"] = "{0}%".format(Img.image_base_width * 100)
            self.tag["width"] = "{0}px".format(image_base_width)
            self.width = image_base_width


class ImgExternal(Img):
    def __init__(self, tag, image_base_width, url_prefix):
        Img.__init__(self, tag, image_base_width)

        # Overwrite image scaling for external SVGs. Size limits are not applied,
        # however display will still be limited to text column width of WP template.
        if self.user_scale > 0:
            self.width = (self.user_scale * self.src_width) - self.image_padding
            self.tag["width"] = "{0}px".format(int(self.width))

        # must have this class
        if (
            "class" in self.tag.parent.attrs.keys()
            and "mefigcentered" in self.tag.parent["class"]  # noqa: W503
        ):
            # read extra information contained in element id
            if "id" in self.tag.parent.attrs.keys():
                for arg in self.tag.parent["id"].split("&"):
                    # extract the remote URL
                    url_op, url_arg = arg.split("=")
                    if url_op == "url":
                        if url_prefix is not None and url_prefix != "":
                            url_arg = url_prefix + "/" + url_arg.replace(".pdf", ".svg")

                        try:
                            r = requests.get(url_arg)
                        except requests.exceptions.ConnectionError:
                            logging.error(
                                "Could not connect to server: {0}".format(url_arg)
                            )
                            return
                        except requests.exceptions.MissingSchema:
                            logging.error(
                                "Schema missing in image URL (http/https). Perhaps you forgot the --url_prefix option?"
                            )
                            return
                        if r.status_code > 400:
                            logging.error("No image found: {0}".format(url_arg))
                        else:
                            self.tag["src"] = url_arg

                        return

        logging.error("External images requested, but no URL provided")
        # exit(1)


class ImgInternal(Img):
    def __init__(self, tag, image_width):
        Img.__init__(self, tag, image_width)
        width = self.width
        height = -1

        # Set HTML size
        #
        # overwrite if size is enforced in pixels
        if self.width > 0:
            width = self.width
            if self.height > 0:
                height = self.height

        # handle physical image data size. Size is limited for inline base64 images
        width = min(width, self.embedded_max_width)
        if height == -1:
            height = width * self.src_height / self.src_width

        self.img = self.img.resize((int(width), int(height)), Image.BICUBIC)

        # Prepare base64 encoded version
        buf = BytesIO()
        # Get image info to preserve transparency
        png_info = self.img.info
        self.img.save(buf, format="PNG", optimize=True, **png_info)
        # Encode base64 and embed in src attribute
        # self.base64 = base64.b64encode(buf.getvalue())
        # Encoding as in RFC 2045
        self.base64 = base64.encodebytes(buf.getvalue())
        # self.base64 = self.base64.ljust(int(math.ceil(len(self.base64) / 4)) * 4, b'=')

        self.tag["src"] = "data:image/png;base64,{0}".format(
            self.base64.decode("utf-8")
        )

        self.tag["alt"] = "See caption below."


class DocComponent(object):
    def __init__(self, heading, menu_order):
        self.id = 0
        self.chapter_id = 0
        self.menu_order = menu_order
        self.siblings = []  # following elements, until end of chapter
        self.heading = str(heading)  # tag element <hN>
        self.title = heading.contents[3]  # hackish way to extract title
        self.title_path = ""

    def __str__(self):
        return self.heading

    def title_to_url_path(self, prefix):
        title = self.title.lower()
        title = title.replace(" ", "-")
        title = title.replace("ä", "ae")
        title = title.replace("ö", "oe")
        title = title.replace("ü", "ue")
        self.title_path = "../../" + prefix + "/" + title

    def get_content(self):
        content = self.heading
        # Process siblings on the same level as <h3>
        for sibling in self.siblings:
            content = content + str(sibling)
        return content

    def get_anchors(self, anchors, prefix="part"):
        """
        Scan content for link anchors. In fact just searches for id's,
        might also return non-anchor id's

        :param anchors: dict for accumulation of anchors
        :param prefix: `part` or `chapter`, inserted in the link path
        :return: list of anchors with current anchors added
        """
        content = self.get_content()
        search_item = 'id="'
        for pos in [m.start() for m in re.finditer(search_item, content)]:
            end = content.find('"', pos + len(search_item))
            anchors[
                "#" + content[pos + len(search_item) : end]  # noqa: E203
            ] = self.title_path
        return anchors

    def fix_references(self, anchors):
        """
        Extend internal links with part type and part URL, because parts will
        end up in separated pages
        :param anchors: list of all anchors in the document
        :return: None
        """
        # logging.debug("search index: {0}".format(anchors))
        for sibling in self.siblings:
            if not isinstance(sibling, bs4.NavigableString):
                for ref in sibling.find_all("a"):
                    if hasattr(ref, "href"):
                        try:
                            if ref["href"].startswith("#"):
                                ref["href"] = "{0}{1}".format(
                                    anchors[ref["href"]], ref["href"]
                                )
                        except KeyError:
                            pass
                        except:  # noqa: E722
                            logging.warning(
                                "Link target not found: {0}".format(ref["href"])
                            )


class Part(DocComponent):
    chapter_count = 100
    """
    Component represented by a <h2> in a Laweb generated HTML
    """

    def __init__(self, content, part_id, menu_order):
        """
        Init part and parse content for h3 chapters

        :param content: soup tag of type h2 and siblings
        :param part_id: unique number for part
        """
        DocComponent.__init__(self, content, menu_order)
        DocComponent.title_to_url_path(self, "part")
        # Content has to be added to part as long as no chapter has been reached
        in_intro = True
        self.id = part_id
        self.chapters = []
        self.chapter_menu_order = 1
        logging.debug("Part: {0} ({1})".format(self.title, part_id))

        for tag in content.next_siblings:
            if tag.name == "h2":
                break
            if tag.name == "h3":
                in_intro = False
                self.chapters.append(
                    Chapter(tag, Part.chapter_count, self.id, self.chapter_menu_order)
                )
                Part.chapter_count += 1
                self.chapter_menu_order += 1
            if in_intro:
                self.siblings.append(tag)


class Chapter(DocComponent):
    """
    Component represented by a <h3> in a Laweb generated HTML
    """

    def __init__(self, content, chapter_id, parent_id, menu_order):
        """
        Read content and all following tags until next h3 begins.

        :param content: soup tag of type h3 and siblings
        :param chapter_id: unique number for chapter
        :param parent_id: id of parent part object
        """
        DocComponent.__init__(self, content, menu_order)
        DocComponent.title_to_url_path(self, "chapter")
        logging.debug(
            "- Chapter: {0} ({1}: {2})".format(self.title, parent_id, chapter_id)
        )

        # Connection for tree structure in Wordpress XML
        self.id = chapter_id
        self.parent = parent_id

        # h3 are chapters from LaWeB to be split, should be separate pages in Pressbooks
        # Hop along the chain of siblings
        tag = content
        for tag in tag.next_siblings:
            if tag.name == "h3" or tag.name == "h2":
                # Next chapter starts here
                break
            self.siblings.append(tag)  # chapter is completely enclosed in <p>


class Laweb(object):
    """
    Can read a HTML file and render it into a Pressbooks/Wordpress XML file.
    """

    def __init__(
        self, html_file, custom_css, external_images, image_width, url_prefix=""
    ):
        """
        Initialize, ead, clean and parse.
        :param html_file: file object providing a HTML file. Tested with valid XHTML files from htlatex
        :param custom_css: file with additional CSS formats
        :param url_prefix: prepended to external images URLs; use if you have ~ in your URL which causes problems in Laweb
        """
        # Root of document tree
        self.parts = []
        self.anchors = {}
        self.external_images = external_images
        self.image_width = image_width
        self.url_prefix = url_prefix

        # CSS prepended to every HTML page
        css_file = open(os.path.join(Path(__file__).parent, "laweb2pressbooks.css"))
        self.css = "<style>{0}</style>".format(css_file.read())
        css_file.close()
        # Add optional CSS file, if defined and present
        if custom_css != "" and custom_css is not None:
            try:
                css_file = open(custom_css)
            except FileNotFoundError as e:  # noqa F841
                logging.error("Custom CSS file not found, skipped")
                css_file = None
            if css_file is not None:
                self.css = self.css + "\n<style>{0}</style>".format(css_file.read())
                css_file.close()

        # Read and basic parsing
        text = html_file.read()
        logging.info("Cleaning source file")
        text = clean_raw(text)
        logging.info("Parsing HTML source")
        self._soup = BeautifulSoup(text, "html.parser")
        logging.info("Cleaning HTML source")
        self.clean_soup()

        try:
            self.author = self._soup.find_all("div", "author")[0].get_text()
        except IndexError:
            self.author = ""

        # Prepare output template
        self.jinja2 = Environment(
            loader=PackageLoader("laweb2pressbooks", "templates"),
            autoescape=select_autoescape(["html", "xml"]),
        )

    def __str__(self):
        template = self.jinja2.get_template("polybook.j2")
        return template.render(doc=self)

    def process_images(self):
        """
        Process images and embed as base64 into the tag object
        """
        for image in self._soup.find_all("img"):
            if self.external_images:
                ImgExternal(image, self.image_width, self.url_prefix)
            else:
                ImgInternal(image, self.image_width)

    def process_external_links(self):
        """
        External links should open in new browser window
        :return:
        """
        for ref in self._soup.find_all("a"):
            try:
                target = ref["href"]
            except KeyError:
                continue
            if str(target).startswith("http"):
                ref["target"] = "_blank"

    def process_applets(self):
        """
        Convert Geogebra links to Geogebra inline applets

        DEPRECATED: use the macro defined in Laweb to create Geogebra applets
        """
        width = 720
        height = 500
        for ref in self._soup.find_all("a"):
            try:
                target = ref["href"]
            except KeyError:
                continue
            if str(target).find("https://ggbm.at/") == -1:
                continue
            logging.warn(
                "Automatic conversion of Geogebra links to applets is deprecated"
            )
            material_id = str(target).split("/")[3]
            new_tag = self._soup.new_tag("iframe")
            new_tag[
                "src"
            ] = "https://www.geogebra.org/material/iframe/id/{material_id}/width/{width}/height/{height}/border/888888/rc/false/ai/false/sdz/false/smb/false/stb/false/stbh/true/ld/false/sri/false".format(
                material_id=material_id, width=width, height=height
            )
            new_tag["scrolling"] = "no"
            new_tag["width"] = "{0}px".format(width)
            new_tag["height"] = "{0}px".format(height)
            new_tag["style"] = "border:0px;"
            ref.parent.insert_before(new_tag)

    def clean_soup(self):
        """
        Preprocess HTML soup before parsing
        :return:
        """
        # Remove all comments
        for element in self._soup(text=lambda text: isinstance(text, Comment)):
            element.extract()

    def parse(self, external_images):
        """
        Build document component tree: parts and chapters, images
        """
        logging.info("Preparing images")
        self.process_images()
        self.process_external_links()

        # Conversion of Geogebra links was deprecated, now removed
        # self.process_applets()

        # h2 will go to parts in Pressbooks
        part_offset = 10  # for item ID, skip default Pressbook parts starting at 1
        part_count = 1
        logging.info("Scanning for parts and chapters")
        for part in self._soup.find_all("h2", class_="chapterHead"):
            self.parts.append(Part(part, part_count + part_offset, part_count))
            part_count += 1

        # References to anchors must be expanded with
        #  - "part/<page_path>"
        #  - "chapter/<page_path>"
        # since source is one file which has been split
        logging.info("Reading anchors")
        anchors = {}
        for part in self.parts:
            anchors = part.get_anchors(anchors, "part")
            for chapter in part.chapters:
                anchors = chapter.get_anchors(anchors, "chapter")

        logging.info("Fixing references")
        for part in self.parts:
            part.fix_references(anchors)
            for chapter in part.chapters:
                chapter.fix_references(anchors)
