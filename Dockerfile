FROM python:3
ARG VCS_TAG

LABEL maintainer="Bengt Giger <bgiger@ethz.ch>"

COPY laweb2pressbooks /app/laweb2pressbooks
COPY requirements.txt /app/
COPY run.sh /usr/local/bin/
RUN chmod 755 /usr/local/bin/run.sh && chown 20913:0 /app/laweb2pressbooks/
ENV PYTHONPATH=/app
WORKDIR /app
RUN echo $VCS_TAG >.version.txt
RUN pip install --proxy=http://proxy.ethz.ch:3128 -r requirements.txt

ENTRYPOINT ["/usr/local/bin/run.sh"]
