Laweb HTML to Wordpress/Pressbooks XML Converter
================================================

This program takes a HTML as produced by *Laweb*, a LaTeX to HTML converter
based on *htlatex*, and outputs a XML file suitable for import in Wordpress
running the Pressbooks plugin.

Main features are:

 - the HTML source is split into parts (h2) and chapters (h3), which become
   separate pages in Pressbooks
 - images are converted to PNG and embedded as base64 encoded strings, to
   avoid the need for maintaining external references
 - the contents of the file `laweb.css` is prepended to each page

Usage
=====

Installation
------------

Install with pip; Python 3 required, use `pip3` if unsure which interpreter is
the default on your system:

```
pip install -U --user laweb2pressbooks --extra-index-url https://gitlab.ethz.ch/api/v4/projects/23024/packages/pypi/simple
```
See below if you want to use a Docker container.

Syntax and Options
------------------

Run with:

```
python -m laweb2pressbooks INPUTFILE
```

or simply

```
laweb2pressbooks INPUTFILE
```

Output is written to `export.xml`, use `--help` for how to set the output name.

Note that documents containing a lot of images may take some time to process.

### Options

Output of `laweb2pressbooks --help`:

- `--output`: export to a specific file; defaults to `export.xml`
- `--cssfile`: add CSS formats from a file; formats are appended to the standard formats, overwriting existing formats
- `--debug`: enable debug output
- `--quiet`: suppress output
- `--image_width`: base width of images (px). Note that the width is restricted to 512px for embedded images, due to
  browser limitations for data embedded in HTML

Docker Container
----------------

The Docker image needs to have access to your working directory with the HTML file.
The example mounts the current directory into the container where the HTML file
is read and the export file will be written. The argument for the mount option `-v`
takes the directory with the source as the first part and the string `/home/workdir`
as the second part:

```
<path_on_your_computer>:/home/workdir
```
Complete commands:
```
docker run --rm -v $(pwd):/home/workdir registry.ethz.ch/laweb/laweb2pressbooks:stable html_source
```
Or with an absolute path on Windows:
```
docker run --rm -v C:\Users\bgiger\Documents\laweb:/home/workdir registry.ethz.ch/laweb/laweb2pressbooks:stable html_source
```
Replace `docker` with `podman` or whatever container runtime you want to use. Remember
that newer images will only be pulled if forced.

Usage Notes
-----------

Don't add a table of content, a navigation menu will be generated.

Introductions and appendices which do not consist of proper parts and chapters
are currently ignored.

Images may be scaled down, because there is a size limit of what
browsers allow as embedded images.

The Pressbooks template must offer a way to disable the Wordpress `wpautop`
function; this function "intelligently" adds paragraphs and is of no use
for a generated source.

The template should not add its own heading numbers, since they already exist
in the HTML coming from Laweb.

The import limit of Wordpress probably has to be raised from the default, 64 MB
should be fine even for large documents.


Developing
=================

Committing a new tag launches a build job which pushes a new package version to
the builtin Gitlab package registry.

During a commit, a pre commit check is performed, defined in `.pre-commit-config.yml`.
The pre commit fixes line endings (forced to Unix style), and does some [reformatting
of the code](https://github.com/psf/black/blob/main/docs/the_black_code_style/current_style.md).

Install the requirements first:

```
pip3 install pre-commit
pre-commit install
```

Then, before committing, optionally run

```
pre-commit run --all-files
```

If pre-commit encounters errors during a commit, it tries to fix them. Commit again
(or run the command above) until all issues are fixed.

On Windows, using PyCharm and a venv:

```
venv\Scripts\pre-commit.exe run --all-files
```
